﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HexCode.EquipmentManager.Kiosk
{
    public abstract class BaseHostViewModel : BaseViewModel
    {
        protected BaseHostViewModel(BaseViewViewModel initialModel)
        {
            this.ActiveViewModel = initialModel;
        }

        protected BaseHostViewModel()
        {
            
        }

        public BaseViewViewModel ActiveViewModel
        {
            get
            {
                return _activeViewModel;
            }

            protected set
            {
                if (_activeViewModel != value)
                {
                    if (_activeViewModel != null)
                        _activeViewModel.Suspend();

                    _activeViewModel = value;
                    RaisePropertyChanged();
                    _activeViewModel.Activate();
                }
            }
        }

        private BaseViewViewModel _activeViewModel;
    }
}
