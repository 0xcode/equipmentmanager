﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HexCode.EquipmentManager.Kiosk
{
    public class MessageBus : IMessageBus
    {
        public MessageBus()
        {
            _subscribersDictionary = new Dictionary<Type, List<object>>();
        }

        public bool Subscribe<TMessage>(Action<TMessage> handler)
        {
            Type messageType = typeof(TMessage);

            if (_subscribersDictionary.ContainsKey(messageType) == false)
            {
                _subscribersDictionary[messageType] = new List<object>();
            }

            _subscribersDictionary[messageType].Add(handler);

            return true;
        }

        public bool Unsubscribe<TMessage>(Action<TMessage> handler)
        {
            Type messageType = typeof(TMessage);
            bool success = false;

            if (_subscribersDictionary.ContainsKey(messageType) == true)
            {
                List<object> handlers = _subscribersDictionary[messageType];

                success = handlers.Remove(handler);

                //Remove the empty list
                if (handlers.Count == 0)
                    _subscribersDictionary.Remove(messageType);
            }

            return success;
        }

        public void Publish<TMessage>(TMessage message)
        {
            Type messageType = typeof(TMessage);

            if (_subscribersDictionary.ContainsKey(messageType) == true)
            {
                IEnumerable<Action<TMessage>> handlers = _subscribersDictionary[messageType].Cast<Action<TMessage>>();

                foreach (Action<TMessage> handler in handlers)
                {
                    handler.Invoke(message);
                }
            }
        }

        private readonly Dictionary<Type, List<object>> _subscribersDictionary;
    }
}