﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using HexCode.EquipmentManager.Data.Services;

namespace HexCode.EquipmentManager.Kiosk
{
    public class KioskUIModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<MessageBus>().As<IMessageBus>();
        }
    }
}
