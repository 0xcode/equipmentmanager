﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace HexCode.EquipmentManager.Kiosk
{
    public abstract class BaseVisibilityConverter<TSource> : IValueConverter
    {
        public virtual object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()) == true)
                return Visibility.Visible;
            else
                return ConvertToVisibility((TSource) value, parameter, culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ConvertFromVisibilty((Visibility) value, parameter, culture);
        }

        protected abstract Visibility ConvertToVisibility(TSource value, object parameter, CultureInfo culture);
        protected abstract TSource ConvertFromVisibilty(Visibility value, object parameter, CultureInfo culture);
    }
}
