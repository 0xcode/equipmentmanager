﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace HexCode.EquipmentManager.Kiosk
{
    public class StringToVisibilityConverter : BaseVisibilityConverter<string>
    {
        protected override Visibility ConvertToVisibility(string value, object parameter, CultureInfo culture)
        {
            return string.IsNullOrEmpty(value) ? Visibility.Hidden : Visibility.Visible;
        }

        protected override string ConvertFromVisibilty(Visibility value, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
