﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace HexCode.EquipmentManager.Kiosk
{
    public class BoolToVisibilityConverter : BaseVisibilityConverter<bool>
    {
        protected override Visibility ConvertToVisibility(bool value, object parameter, CultureInfo culture)
        {
            return (value ? Visibility.Visible : Visibility.Hidden);
        }

        protected override bool ConvertFromVisibilty(Visibility value, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
