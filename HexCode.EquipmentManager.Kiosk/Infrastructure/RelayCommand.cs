﻿using System;
using System.Windows.Input;

namespace HexCode.EquipmentManager.Kiosk
{
    public class RelayCommand : ICommand
    {
        public RelayCommand(Action executeDelegate)
            : this(x => executeDelegate(), x => { return true; })
        {
        }

        public RelayCommand(Action executeDelegate, Func<bool> canExecuteDelegate) 
        : this(x => executeDelegate.Invoke(), x => canExecuteDelegate.Invoke())
        {
        }

        public RelayCommand(Action<object> executeDelegate)
            : this(executeDelegate, o => { return true; })
        {
        }

        public RelayCommand(Action<object> executeDelegate, Func<object, bool> canExecuteDelegate)
        {
            _executeDelegate = executeDelegate;
            _canExecuteDelegate = canExecuteDelegate;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecuteDelegate.Invoke(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public void Execute(object parameter)
        {
            _executeDelegate.Invoke(parameter);
        }

        private readonly Func<object, bool> _canExecuteDelegate;

        private readonly Action<object> _executeDelegate;
    }
}