﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Autofac;
using Autofac.Core;
using HexCode.EquipmentManager.Data.EntityFramework;
using HexCode.EquipmentManager.Data.Infrastructure;
using HexCode.EquipmentManager.Kiosk.ViewModels;

namespace HexCode.EquipmentManager.Kiosk
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule(new DataDependencyInjectionModule("")); //TODO [Andrew]: Pull connection string
            builder.RegisterModule(new KioskUIModule());

            builder.RegisterType<MainWindowViewModel>();

            _container = builder.Build();
        }

        public MainWindowViewModel MainWindowViewModel
        {
            get
            {
                return _container.Resolve<MainWindowViewModel>();
            }
        }

        private readonly IContainer _container;
    }
}
