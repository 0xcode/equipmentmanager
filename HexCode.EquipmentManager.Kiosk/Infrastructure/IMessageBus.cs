﻿using System;

namespace HexCode.EquipmentManager.Kiosk
{
    public interface IMessageBus
    {
        bool Subscribe<TMessage>(Action<TMessage> handler);
        bool Unsubscribe<TMessage>(Action<TMessage> handler);
        void Publish<TMessage>(TMessage message);
    }
}