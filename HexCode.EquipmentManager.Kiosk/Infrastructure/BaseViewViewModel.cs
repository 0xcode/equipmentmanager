﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HexCode.EquipmentManager.Kiosk
{
    public abstract class BaseViewViewModel : BaseViewModel
    {
        public void Suspend()
        {
            PreSuspend();

            this.IsActive = false;

            PostActivate();
        }

        public void Activate()
        {
            PreActivate();

            this.IsActive = true;

            PostActivate();
        }

        protected virtual void PreSuspend() { /*Intentionally left empty*/ }
        protected virtual void PostActivate() { /*Intentionally left empty*/ }

        protected virtual void PreActivate() { /*Intentionally left empty*/ }
        protected virtual void PostResume() { /*Intentionally left empty*/ }

        public bool IsActive
        {
            get
            {
                return _isActive; 

            }
            private set
            {
                _isActive = value;
                RaisePropertyChanged();
            }
        }

        private bool _isActive;
    }
}
