﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HexCode.EquipmentManager.Core.Models;

namespace HexCode.EquipmentManager.Kiosk.Messages
{
    public class UserLoginMessage
    {
        public UserLoginMessage(User user)
        {
            this.User = user;
        }

        public User User { get; private set; }
    }
}
