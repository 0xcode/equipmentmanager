﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HexCode.EquipmentManager.Kiosk.Messages
{
    public class ViewUserMessage
    {
        public ViewUserMessage(string userId)
        {
            this.UserId = userId;
        }

        public string UserId { get; private set; }
    }
}
