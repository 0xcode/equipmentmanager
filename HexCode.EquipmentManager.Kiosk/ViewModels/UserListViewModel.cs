﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using HexCode.EquipmentManager.Data.Services;
using HexCode.EquipmentManager.Kiosk.Messages;

namespace HexCode.EquipmentManager.Kiosk.ViewModels
{
    public class UserListViewModel : BaseViewViewModel
    {
        public UserListViewModel(IMessageBus messageBus, IUserService userService)
        {
            _messageBus = messageBus;
            _userService = userService;
            _viewUserCommand = new RelayCommand(ViewUser, CanViewUser);
            LoadUsers();
        }

        public UserListItemViewModel SelectedUser
        {
            get
            {
                return _selectedUser;
            }
            set
            {
                _selectedUser = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<UserListItemViewModel> Users
        {
            get
            {
                return _users;
            }
            private set
            {
                _users = value;
                RaisePropertyChanged();
            }
        }

        public ICommand ViewUserCommand
        {
            get
            {
                return _viewUserCommand;
            }
        }

        protected override void PreActivate()
        {
            LoadUsers();
        }

        private bool CanViewUser()
        {
            return _selectedUser != null;
        }

        private void LoadUsers()
        {
            var userList = _userService.ListAll().Select(u => new UserListItemViewModel(_messageBus, u));

            this.SelectedUser = null;
            this.Users = new ObservableCollection<UserListItemViewModel>(userList);
        }

        private void ViewUser()
        {
            ViewUserMessage message = new ViewUserMessage(this.SelectedUser.Id);

            _messageBus.Publish(message);
        }

        private readonly IMessageBus _messageBus;
        private readonly IUserService _userService;
        private readonly ICommand _viewUserCommand;

        private UserListItemViewModel _selectedUser;

        private ObservableCollection<UserListItemViewModel> _users;
    }
}