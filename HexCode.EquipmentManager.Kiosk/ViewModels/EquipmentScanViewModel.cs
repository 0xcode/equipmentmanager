﻿using System.Windows.Input;
using HexCode.EquipmentManager.Core.Enums;
using HexCode.EquipmentManager.Core.Models;
using HexCode.EquipmentManager.Data.Services;

namespace HexCode.EquipmentManager.Kiosk.ViewModels
{
    public class EquipmentScanViewModel : BaseViewViewModel
    {
        public EquipmentScanViewModel(IMessageBus messageBus, IEquipmentService equipmentService, User user)
        {
            _messageBus = messageBus;
            _equipmentService = equipmentService;
            _user = user;
            _findItemCommand = new RelayCommand(FindItem);
            _checkoutCommand = new RelayCommand(CheckoutItem, CanCheckoutItem);
        }

        public string SearchValue
        {
            get
            {
                return _searchValue;
            }

            set
            {
                _searchValue = value;
                RaisePropertyChanged();
            }
        }

        public bool HasScannedItem
        {
            get
            {
                return _foundItem != null;
            }
        }

        public ICommand FindItemCommand
        {
            get
            {
                return _findItemCommand;
            }
        }

        public ICommand CheckoutCommand
        {
            get
            {
                return _checkoutCommand;
            }
        }

        private void CheckoutItem()
        {
            //TODO
        }

        private bool CanCheckoutItem()
        {
            return _user.Permissions.HasFlag(UserPermissions.SelfCheckOut);
        }

        private void FindItem()
        {
            //TODO
        }

        private readonly IMessageBus _messageBus;
        private readonly IEquipmentService _equipmentService;
        private readonly User _user;
        private readonly ICommand _findItemCommand;
        private readonly ICommand _checkoutCommand;
        private EquipmentItem _foundItem;
        private string _searchValue;
    }
}