﻿using System.Windows.Input;
using HexCode.EquipmentManager.Core.Models;
using HexCode.EquipmentManager.Kiosk.Messages;

namespace HexCode.EquipmentManager.Kiosk.ViewModels
{
    public class UserListItemViewModel : BaseViewModel
    {
        public UserListItemViewModel(IMessageBus messageBus) : this(messageBus, new User())
        {
        }

        public UserListItemViewModel(IMessageBus messageBus, User model)
        {
            _messageBus = messageBus;
            _model = model;
            _viewDetailsCommand = new RelayCommand(ViewDetails);
        }

        public string FirstName
        {
            get
            {
                return _model.FirstName;
            }
        }

        public string LastName
        {
            get
            {
                return _model.LastName;
            }
        }


        public string Id
        {
            get
            {
                return _model.Id;
            }
        }

        public ICommand ViewDetailsCommand
        {
            get
            {
                return _viewDetailsCommand;
            }
        }

        private void ViewDetails()
        {
            ViewUserMessage message = new ViewUserMessage(_model.Id);

            _messageBus.Publish(message);
        }

        private readonly ICommand _viewDetailsCommand;
        private readonly IMessageBus _messageBus;
        private readonly User _model;
    }
}