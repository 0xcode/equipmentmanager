﻿using System.Collections.Generic;
using System.Windows.Input;
using HexCode.EquipmentManager.Core.Models;
using HexCode.EquipmentManager.Data.Services;
using HexCode.EquipmentManager.Kiosk.Messages;

namespace HexCode.EquipmentManager.Kiosk.ViewModels
{
    public class MainWindowViewModel : BaseHostViewModel
    {
        public MainWindowViewModel(IMessageBus messageBus, IUserService userService, IEquipmentService equipmentService)
        {
            _messageBus = messageBus;
            _userService = userService;
            _equipmentService = equipmentService;
            _viewHistory = new Stack<BaseViewViewModel>();
            _authenticationView = new UserScanViewModel(_messageBus, _userService);
            this.ActiveViewModel = _authenticationView;

            _logoutCommand = new RelayCommand(UserLogout);
            _viewUserListCommand = new RelayCommand(ViewUserList);

            InitializeMessageHandlers();
        }

        public bool IsUserLoggedIn
        {
            get
            {
                return _authenticatedUser != null;
            }
        }

        public string WelcomeMessage
        {
            get
            {
                if (_authenticatedUser != null)
                    return string.Format(Resources.MainWindow.WelcomeMessage, _authenticatedUser.FirstName);
                else
                    return string.Empty;
            }
        }

        public ICommand LogoutCommand
        {
            get
            {
                return _logoutCommand;
            }
        }

        public ICommand ViewUserListCommand
        {
            get
            {
                return _viewUserListCommand;
            }
        }

        private void HandleBackMessage(ViewPreviousMessage message)
        {
            if (_viewHistory.Count > 0)
            {
                BaseViewViewModel model = _viewHistory.Pop();

                this.ActiveViewModel = model;
            }
        }

        private void HandleUserLoginMessage(UserLoginMessage message)
        {
            _authenticatedUser = message.User;
            NotifyUserChanged();

            //Set the active view to be the scan page and pass the active user
            this.ActiveViewModel = new EquipmentScanViewModel(_messageBus, _equipmentService, _authenticatedUser);
        }

        private void UserLogout()
        {
            _authenticatedUser = null;
            NotifyUserChanged();

            this.ActiveViewModel = _authenticationView;
        }

        private void ViewUserList()
        {
            this.ActiveViewModel = new UserListViewModel(_messageBus, _userService);
        }

        private void InitializeMessageHandlers()
        {
            _messageBus.Subscribe<UserLoginMessage>(HandleUserLoginMessage);
            _messageBus.Subscribe<ViewPreviousMessage>(HandleBackMessage);
        }

        private void NotifyUserChanged()
        {
            RaisePropertyChanged(nameof(this.IsUserLoggedIn));
            RaisePropertyChanged(nameof(this.WelcomeMessage));
        }

        private readonly ICommand _logoutCommand;
        private readonly ICommand _viewUserListCommand;
        private readonly IEquipmentService _equipmentService;
        private readonly IMessageBus _messageBus;
        private readonly IUserService _userService;
        private readonly Stack<BaseViewViewModel> _viewHistory;
        private User _authenticatedUser;

        private readonly BaseViewViewModel _authenticationView;
    }
}