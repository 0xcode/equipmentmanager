﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HexCode.EquipmentManager.Core.Models;

namespace HexCode.EquipmentManager.Kiosk.ViewModels
{
    public class EquipmentDetailsViewModel : BaseViewModel
    {
        private readonly EquipmentItem _model;

        public EquipmentDetailsViewModel(EquipmentItem model)
        {
            _model = model;
        }

        public string Name { get; set; }
    }
}
