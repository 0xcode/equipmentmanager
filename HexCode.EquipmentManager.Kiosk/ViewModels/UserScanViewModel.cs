﻿using System;
using System.Windows.Input;
using HexCode.EquipmentManager.Core.Models;
using HexCode.EquipmentManager.Data.Services;
using HexCode.EquipmentManager.Kiosk.Messages;

namespace HexCode.EquipmentManager.Kiosk.ViewModels
{
    public class UserScanViewModel : BaseViewViewModel
    {
        public UserScanViewModel(IMessageBus messageBus, IUserService userService)
        {
            _messageBus = messageBus;
            _userService = userService;
            _findUserCommand = new RelayCommand(FindUser);
            _keyInputCommand = new RelayCommand(KeyInput);
        }

        public string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
            set
            {
                _errorMessage = value;
                RaisePropertyChanged();
            }
        }

        public ICommand InputCommand
        {
            get
            {
                return _keyInputCommand;
            }
        }

        public string ScanValue
        {
            get
            {
                return _scanValue;
            }
            set
            {
                _scanValue = value;
                RaisePropertyChanged();
            }
        }

        public ICommand FindUserCommand
        {
            get
            {
                return _findUserCommand;
            }
        }

        protected override void PreActivate()
        {
            this.ScanValue = null;
        }

        private void KeyInput(object parameter)
        {
            Console.WriteLine(parameter);
        }

        private void FindUser()
        {
            if (string.IsNullOrEmpty(this.ScanValue) == false)
            {
                User user = _userService.Get(this.ScanValue);

                if (user != null)
                {
                    UserLoginMessage message = new UserLoginMessage(user);
                    _messageBus.Publish(message);
                }
            }
            else
            {
                this.ErrorMessage = "You must enter a value!";
            }
        }

        private readonly ICommand _keyInputCommand;
        private readonly IMessageBus _messageBus;
        private readonly IUserService _userService;
        private readonly ICommand _findUserCommand;
        private string _errorMessage;

        private string _scanValue;
    }
}