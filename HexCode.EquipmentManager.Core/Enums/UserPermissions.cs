﻿using System;

namespace HexCode.EquipmentManager.Core.Enums
{
    public enum UserPermissions
    {
        None = 0,
        SelfCheckOut = 1 << 0,
        SelfCheckIn = 1 << 1,
        OtherCheckOut = 1 << 2,
        OtherCheckIn = 1 << 3,
        All = Int32.MaxValue
    }
}