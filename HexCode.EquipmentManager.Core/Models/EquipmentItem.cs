﻿namespace HexCode.EquipmentManager.Core.Models
{
    public class EquipmentItem
    {
        public string Description { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string SerialNumber { get; set; }
    }
}