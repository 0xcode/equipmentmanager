﻿namespace HexCode.EquipmentManager.Core.Models
{
    public class EquipmentType
    {
        public string Description { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public EquipmentType Type { get; set; }
    }
}