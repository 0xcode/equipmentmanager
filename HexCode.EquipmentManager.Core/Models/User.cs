﻿using HexCode.EquipmentManager.Core.Enums;

namespace HexCode.EquipmentManager.Core.Models
{
    public class User : BaseModel
    {
        public string FirstName { get; set; }
        public string Id { get; set; }
        public string LastName { get; set; }
        public UserPermissions Permissions { get; set; }

        public bool IsActive { get; set; }
        public override ModelValidationResult Validate()
        {
            ModelValidationResult result = new ModelValidationResult();

            if (string.IsNullOrEmpty(this.FirstName) == true)
                result.AddError(nameof(this.FirstName), Resources.User.FirstNameEmptyError);

            if (string.IsNullOrEmpty(this.LastName) == true)
                result.AddError(nameof(this.LastName), Resources.User.LastNameEmptyError);

            return result;
        }
    }
}