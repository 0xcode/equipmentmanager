﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HexCode.EquipmentManager.Core
{
    public class ModelValidationResult
    {
        public ModelValidationResult()
        {
            _fieldErrors = new Dictionary<string, string>();
        }

        public void AddError(string fieldName, string errorMessage, params object[] args)
        {
            string formattedError = string.Format(errorMessage, args);

            if (_fieldErrors.ContainsKey(fieldName) == true)
                _fieldErrors[fieldName] = formattedError;
            else
                _fieldErrors.Add(fieldName, formattedError);
        }

        public bool IsValid
        {
            get
            {
                return _fieldErrors.Values.Count == 0;
            }
        }

        private readonly Dictionary<string, string> _fieldErrors;
    }
}
