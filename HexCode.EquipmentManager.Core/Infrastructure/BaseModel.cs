﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HexCode.EquipmentManager.Core
{
    public abstract class BaseModel
    {
        public abstract ModelValidationResult Validate();
    }
}
