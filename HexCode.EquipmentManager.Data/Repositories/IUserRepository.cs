﻿using System.Collections.Generic;
using HexCode.EquipmentManager.Core.Models;
using HexCode.EquipmentManager.Data.Infrastructure;
using HexCode.EquipmentManager.Data.SearchCriteria;

namespace HexCode.EquipmentManager.Data.Repositories
{
    internal interface IUserRepository
    {
        User Get(string id);
        List<User> ListAll();
        PagedList<User> List(PagingInfo pagingInfo, UserFilteringInfo filteringInfo);
        void Create(User toCreate);
        void Update(User toUpdate);
        void Delete(User toDelete);
    }
}