﻿using System.Linq;

namespace HexCode.EquipmentManager.Data.SearchCriteria
{
    public class UserFilteringInfo : FilteringInfo
    {
        public enum UserStateFilter
        {
            All,
            ActiveOnly,
            InactiveOnly
        }

        public UserStateFilter UserState { get; set; }
    }
}