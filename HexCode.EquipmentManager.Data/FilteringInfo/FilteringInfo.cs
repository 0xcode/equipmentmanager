﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace HexCode.EquipmentManager.Data
{
    public enum SortDirection
    {
        Ascending,
        Descending
    }

    public class FilteringInfo
    {
        public string FilterText { get; set; }
        public SortDirection SortedDirection { get; set; }

        public string SortedProperty { get; set; }
    }
}