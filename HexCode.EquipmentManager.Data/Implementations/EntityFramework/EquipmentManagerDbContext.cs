﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HexCode.EquipmentManager.Core.Enums;
using HexCode.EquipmentManager.Core.Models;
using HexCode.EquipmentManager.Data.EntityFramework.Repositories;
using Microsoft.EntityFrameworkCore;

namespace HexCode.EquipmentManager.Data.EntityFramework
{
    internal class EquipmentManagerDbContext : DbContext
    {
        private static bool DatabaseCreateChecked = false;

        public EquipmentManagerDbContext(string connectionString) : base()
        {
            _connectionString = connectionString;

            if (DatabaseCreateChecked == false)
            {
                base.Database.EnsureCreated();
                DatabaseCreateChecked = true;
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().HasData(
                new User() { Id = Guid.NewGuid().ToString(), FirstName = "FirstName1", LastName = "LastName1", IsActive = true, Permissions = UserPermissions.All },
                new User() { Id = Guid.NewGuid().ToString(), FirstName = "FirstName2", LastName = "LastName2", IsActive = true, Permissions = UserPermissions.All },
                new User() { Id = Guid.NewGuid().ToString(), FirstName = "FirstName3", LastName = "LastName3", IsActive = false, Permissions = UserPermissions.All },
                new User() { Id = Guid.NewGuid().ToString(), FirstName = "FirstName4", LastName = "LastName4", IsActive = true, Permissions = UserPermissions.All },
                new User() { Id = Guid.NewGuid().ToString(), FirstName = "FirstName4", LastName = "LastName4", IsActive = true, Permissions = UserPermissions.All },
                new User() { Id = Guid.NewGuid().ToString(), FirstName = "FirstName5", LastName = "LastName5", IsActive = true, Permissions = UserPermissions.All },
                new User() { Id = Guid.NewGuid().ToString(), FirstName = "FirstName6", LastName = "LastName6", IsActive = true, Permissions = UserPermissions.All },
                new User() { Id = Guid.NewGuid().ToString(), FirstName = "FirstName7", LastName = "LastName7", IsActive = true, Permissions = UserPermissions.All }
            );
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }

        private readonly string _connectionString;
    }
}
