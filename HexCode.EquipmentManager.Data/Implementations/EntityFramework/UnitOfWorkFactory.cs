﻿namespace HexCode.EquipmentManager.Data.EntityFramework
{
    internal class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        public UnitOfWorkFactory(string nameOrConnectionString)
        {
            _nameOrConnectionString = nameOrConnectionString;
        }

        public IUnitOfWork Create()
        {
            return new UnitOfWork(_nameOrConnectionString);
        }

        private readonly string _nameOrConnectionString;
    }
}