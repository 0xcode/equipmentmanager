﻿using System;
using System.Collections.Generic;
using HexCode.EquipmentManager.Data.EntityFramework.Repositories;
using HexCode.EquipmentManager.Data.Repositories;

namespace HexCode.EquipmentManager.Data.EntityFramework
{
    internal class RepositoryFactory : IRepositoryFactory
    {
        static RepositoryFactory()
        {
            //Create the dictionary that will store our repository creator methods
            RepositoryDictionary = new Dictionary<Type, Func<IUnitOfWork, BaseRepository>>
            {
                {typeof(IUserRepository), unitOfWork => new UserRepository(unitOfWork)}
            };
        }

        public TRepository Create<TRepository>(IUnitOfWork unitOfWork) where TRepository : class
        {
            Type repositoryType = typeof(TRepository);

            if (RepositoryDictionary.ContainsKey(repositoryType) == false)
                throw new InvalidOperationException(string.Format("No repository that matches type: {0}", repositoryType));

            return RepositoryDictionary[repositoryType].Invoke(unitOfWork) as TRepository;
        }

        private static readonly Dictionary<Type, Func<IUnitOfWork, BaseRepository>> RepositoryDictionary;
    }
}