﻿using System;
using System.Collections.Generic;
using System.Linq;
using HexCode.EquipmentManager.Core.Models;
using HexCode.EquipmentManager.Data.Infrastructure;
using HexCode.EquipmentManager.Data.Repositories;
using HexCode.EquipmentManager.Data.SearchCriteria;
using Microsoft.EntityFrameworkCore;

namespace HexCode.EquipmentManager.Data.EntityFramework.Repositories
{
    internal class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(DbContext context)
            : base(context)
        {
        }

        public PagedList<User> List(PagingInfo pagingInfo, UserFilteringInfo filteringInfo)
        {
            IQueryable<User> query = _rootSet.AsQueryable();

            //Create the filter
            query = query.Filter(filteringInfo.FilterText, u => u.FirstName, u => u.LastName);

            if (filteringInfo.UserState == UserFilteringInfo.UserStateFilter.ActiveOnly)
                query.Where(u => u.IsActive == true);
            else if (filteringInfo.UserState == UserFilteringInfo.UserStateFilter.InactiveOnly)
                query.Where(u => u.IsActive == false);


            //We need to order the list, either by the defined filter criteria or ID by default
            if (String.IsNullOrEmpty(filteringInfo.SortedProperty) == false)
                query = query.OrderBy(filteringInfo);
            else
                query = query.OrderBy(u => u.FirstName);

            //Return the list paged
            return query.ToPagedList(pagingInfo);
        }

        public void Create(User toCreate)
        {
            base.Insert(toCreate);
        }

        public void Delete(User toDelete)
        {
            base.Remove(toDelete);
        }

        public User Get(string id)
        {
            return _rootSet.FirstOrDefault(u => u.Id == id);
        }

        public List<User> ListAll()
        {
            return _rootSet.ToList();
        }

        public void Update(User toUpdate)
        {
            base.Modify(toUpdate);
        }
    }
}