﻿using System.Collections.Generic;
using HexCode.EquipmentManager.Core;
using Microsoft.EntityFrameworkCore;

namespace HexCode.EquipmentManager.Data.EntityFramework.Repositories
{
    internal abstract class BaseRepository<TEntity> where TEntity : BaseModel
    {
        protected BaseRepository(DbContext context)
        {
            _context = context;
            _rootSet = _context.Set<TEntity>();
        }

        protected void Insert(TEntity entity)
        {
            _rootSet.Add(entity);
        }

        protected void Insert(IEnumerable<TEntity> entities)
        {
            _rootSet.AddRange(entities);
        }

        protected void Insert<TOtherEntity>(TOtherEntity entity) where TOtherEntity : BaseModel
        {
            DbSet<TOtherEntity> set = CreateSet<TOtherEntity>();
            set.Add(entity);
        }

        protected void Insert<TOtherEntity>(IEnumerable<TOtherEntity> entity) where TOtherEntity : BaseModel
        {
            DbSet<TOtherEntity> set = CreateSet<TOtherEntity>();
            set.AddRange(entity);
        }

        protected DbSet<TOtherEntity> CreateSet<TOtherEntity>() where TOtherEntity : BaseModel
        {
            return _context.Set<TOtherEntity>();
        }

        protected void Remove(TEntity entity)
        {
            _rootSet.Remove(entity);
        }

        protected void Remove(IEnumerable<TEntity> entities)
        {
            _rootSet.RemoveRange(entities);
        }

        protected void Remove<TOtherEntity>(TOtherEntity entity) where TOtherEntity : BaseModel
        {
            DbSet<TOtherEntity> set = CreateSet<TOtherEntity>();
            set.Remove(entity);
        }

        protected void Remove<TOtherEntity>(IEnumerable<TOtherEntity> entity) where TOtherEntity : BaseModel
        {
            DbSet<TOtherEntity> set = CreateSet<TOtherEntity>();
            set.RemoveRange(entity);
        }

        protected void Modify(TEntity entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
                _rootSet.Attach(entity);

            _context.Entry(entity).State = EntityState.Modified;
        }

        protected void Modify<TOtherEntity>(TOtherEntity entity) where TOtherEntity : BaseModel
        {
            DbSet<TOtherEntity> set = CreateSet<TOtherEntity>();

            if (_context.Entry(entity).State == EntityState.Detached)
                set.Attach(entity);

            _context.Entry(entity).State = EntityState.Modified;
        }

        protected readonly DbSet<TEntity> _rootSet;
        private readonly DbContext _context;
    }
}