﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HexCode.EquipmentManager.Data.EntityFramework.Repositories;
using HexCode.EquipmentManager.Data.Repositories;
using Microsoft.EntityFrameworkCore;

namespace HexCode.EquipmentManager.Data.EntityFramework
{
    internal class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(string nameOrConnectionString)
        {
            _context = new EquipmentManagerDbContext(nameOrConnectionString);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public IUserRepository UserRepository
        {
            get
            {
                return (_userRepository ?? (_userRepository = new UserRepository(_context)));
            }
        }

        public void Dispose()
        {
            if(_context != null)
                _context.Dispose();
        }

        public DbContext Context
        {
            get
            {
                return _context;
            }
        }

        private readonly DbContext _context;
        private IUserRepository _userRepository;
    }
}
