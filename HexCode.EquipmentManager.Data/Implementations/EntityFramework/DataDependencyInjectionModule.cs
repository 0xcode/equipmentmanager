﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using HexCode.EquipmentManager.Data.Infrastructure;

namespace HexCode.EquipmentManager.Data.EntityFramework
{
    public class DataDependencyInjectionModule : BaseDataDependencyInjectionModule
    {
        public DataDependencyInjectionModule(string nameOrConnectionString)
        {
            _nameOrConnectionString = nameOrConnectionString;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterTypeWithNonPublicConstructors<UnitOfWorkFactory>().As<IUnitOfWorkFactory>().WithParameter(new TypedParameter(typeof(string), _nameOrConnectionString));
        }

        private readonly string _nameOrConnectionString;
    }
}
