﻿using System.Collections.Generic;
using HexCode.EquipmentManager.Core.Models;
using HexCode.EquipmentManager.Data.Infrastructure;
using HexCode.EquipmentManager.Data.SearchCriteria;

namespace HexCode.EquipmentManager.Data.Services
{
    public interface IUserService
    {
        Result Create(User toCreate);
        Result Delete(string id);
        User Get(string id);
        List<User> ListAll();
        PagedList<User> List(PagingInfo pagingInfo, UserFilteringInfo filteringInfo = null);
        Result Update(User toUpdate);
    }
}