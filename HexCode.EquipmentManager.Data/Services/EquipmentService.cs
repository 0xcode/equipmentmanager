﻿using System;
using System.Collections.Generic;
using HexCode.EquipmentManager.Core.Models;

namespace HexCode.EquipmentManager.Data.Services
{
    public class EquipmentService : BaseService, IEquipmentService
    {
        internal EquipmentService(IUnitOfWorkFactory unitOfWorkFactory)
            : base(unitOfWorkFactory)
        {
        }

        public Result Create(EquipmentItem toCreate)
        {
            throw new NotImplementedException();
        }

        public Result Delete(string id)
        {
            throw new NotImplementedException();
        }

        public EquipmentItem Get(string id)
        {
            throw new NotImplementedException();
        }

        public List<EquipmentItem> List()
        {
            throw new NotImplementedException();
        }

        public Result Update(EquipmentItem toUpdate)
        {
            throw new NotImplementedException();
        }

        private static readonly List<EquipmentItem> MockStore = new List<EquipmentItem>()
        {
            new EquipmentItem() {Description = "A test object", Id = "E123", Name = "Test Item 1", SerialNumber = "E123"},
            new EquipmentItem() {Description = "Another test objec", Id = "E123", Name = "Test Item 2", SerialNumber = "E321"}
        };
    }
}