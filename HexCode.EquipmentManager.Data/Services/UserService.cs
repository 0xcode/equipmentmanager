﻿using System.Collections.Generic;
using HexCode.EquipmentManager.Core;
using HexCode.EquipmentManager.Core.Models;
using HexCode.EquipmentManager.Data.Infrastructure;
using HexCode.EquipmentManager.Data.SearchCriteria;

namespace HexCode.EquipmentManager.Data.Services
{
    public class UserService : BaseService, IUserService
    {
        internal UserService(IUnitOfWorkFactory unitOfWorkFactory)
            : base(unitOfWorkFactory)
        {
        }

        public Result Create(User toCreate)
        {
            Result toReturn;

            ModelValidationResult validationResult = toCreate.Validate();

            if (validationResult.IsValid == true)
            {
                using (IUnitOfWork unitOfWork = CreateUnitOfWork())
                {
                    unitOfWork.UserRepository.Create(toCreate);

                    unitOfWork.Save();
                }

                toReturn = Result.Success();
            }
            else
            {
                toReturn = Result.Error(Resources.UserService.ModelInvalidError);
            }

            return toReturn;
        }

        public Result Delete(string id)
        {
            using (IUnitOfWork unitOfWork = CreateUnitOfWork())
            {
                User toDelete = unitOfWork.UserRepository.Get(id);

                if (toDelete != null)
                {
                    unitOfWork.UserRepository.Delete(toDelete);

                    unitOfWork.Save();
                }
            }

            return Result.Success();
        }

        public User Get(string id)
        {
            using (IUnitOfWork unitOfWork = CreateUnitOfWork())
            {
                return unitOfWork.UserRepository.Get(id);
            }
        }

        public List<User> ListAll()
        {
            using (IUnitOfWork unitOfWork = CreateUnitOfWork())
            {
                return unitOfWork.UserRepository.ListAll();
            }
        }

        public PagedList<User> List(PagingInfo pagingInfo, UserFilteringInfo filteringInfo = null)
        {
            using (IUnitOfWork unitOfWork = CreateUnitOfWork())
            {
                return unitOfWork.UserRepository.List(pagingInfo, filteringInfo);
            }
        }

        public Result Update(User toUpdate)
        {
            Result toReturn;

            ModelValidationResult validationResult = toUpdate.Validate();

            if (validationResult.IsValid == true)
            {
                using (IUnitOfWork unitOfWork = CreateUnitOfWork())
                {
                    unitOfWork.UserRepository.Update(toUpdate);

                    unitOfWork.Save();
                }

                toReturn = Result.Success();
            }
            else
            {
                toReturn = Result.Error(Resources.UserService.ModelInvalidError);
            }

            return toReturn;
        }
    }
}