﻿namespace HexCode.EquipmentManager.Data
{
    public abstract class BaseService
    {
        internal BaseService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        internal IUnitOfWork CreateUnitOfWork()
        {
            return _unitOfWorkFactory.Create();
        }

        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
    }
}