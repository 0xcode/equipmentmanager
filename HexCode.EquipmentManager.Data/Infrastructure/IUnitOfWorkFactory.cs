﻿namespace HexCode.EquipmentManager.Data
{
    internal interface IUnitOfWorkFactory
    {
        IUnitOfWork Create();
    }
}