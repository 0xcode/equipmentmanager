﻿using System;
using System.Collections.Concurrent;
using System.Reflection;
using Autofac;
using Autofac.Builder;
using Autofac.Core.Activators.Reflection;
using HexCode.EquipmentManager.Data.Services;
using Module = Autofac.Module;

namespace HexCode.EquipmentManager.Data.Infrastructure
{
    public class NonPublicConstructorFinder : IConstructorFinder
    {
        public ConstructorInfo[] FindConstructors(Type targetType)
        {
            ConstructorInfo[] result = ConstructorCache.GetOrAdd(targetType, t => t.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public));

            if (result.Length == 0)
                throw new NoConstructorsFoundException(targetType);

            return result;
        }

        private static readonly ConcurrentDictionary<Type, ConstructorInfo[]> ConstructorCache = new ConcurrentDictionary<Type, ConstructorInfo[]>();
    }

    public static class AutofacExtensions
    {
        public static IRegistrationBuilder<TImplementer, ConcreteReflectionActivatorData, SingleRegistrationStyle> RegisterTypeWithNonPublicConstructors<TImplementer>(this ContainerBuilder builder)
        {
            return builder.RegisterType<TImplementer>().FindConstructorsWith(new NonPublicConstructorFinder());
        }
    }

    public abstract class BaseDataDependencyInjectionModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterTypeWithNonPublicConstructors<UserService>().As<IUserService>();
            builder.RegisterTypeWithNonPublicConstructors<EquipmentService>().As<IEquipmentService>();
        }
    }
}