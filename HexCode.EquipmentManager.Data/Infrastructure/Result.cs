﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HexCode.EquipmentManager.Data
{
    public class Result
    {
        public static Result Success()
        {
            return new Result(true, null);
        }

        public static Result Error(string message)
        {
            return new Result(false, message);
        }

        public static Result Error(string message, params object[] args)
        {
            return new Result(false, string.Format(message, args));
        }

        protected Result(bool value, string errorMessage)
        {
            this.Value = value;
            this.ErrorMessage = errorMessage;
        }

        public bool Value { get; private set; }
        public string ErrorMessage { get; private set; }
    }
}
