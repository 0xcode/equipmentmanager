﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HexCode.EquipmentManager.Data.Infrastructure
{
    public class PagingInfo
    {
        public PagingInfo(int pageSize, int pageNumber)
        {
            this.PageSize = pageSize;
            this.PageNumber = pageNumber;
        }

        public int PageSize { get; private set; }
        public int PageNumber { get; private set; }
    }

    public class PagedList<TData> : IEnumerable<TData>
    {
        public PagedList(PagingInfo pagingInfo, int itemCount, IEnumerable<TData> items)
        {
            this.Items = items;
            this.ItemCount = itemCount;
            this.PageNumber = pagingInfo.PageNumber;
            this.PageSize = pagingInfo.PageSize;
        }

        public int PageSize { get; private set; }
        public int ItemCount { get; private set; }
        public int PageNumber { get; private set; }

        public int PageCount
        {
            get
            {
                return (int)(Math.Ceiling(this.ItemCount / (double)this.PageSize));
            }
        }

        public IEnumerable<TData> Items { get; private set; }

        public IEnumerator<TData> GetEnumerator()
        {
            return this.Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
