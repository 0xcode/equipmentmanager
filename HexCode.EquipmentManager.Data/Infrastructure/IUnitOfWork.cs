﻿using System;
using HexCode.EquipmentManager.Data.Repositories;

namespace HexCode.EquipmentManager.Data
{
    internal interface IUnitOfWork : IDisposable
    {
        IUserRepository UserRepository { get; }
        void Save();
    }
}