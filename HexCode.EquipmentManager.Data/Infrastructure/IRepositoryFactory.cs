﻿namespace HexCode.EquipmentManager.Data
{
    internal interface IRepositoryFactory
    {
        TRepository Create<TRepository>(IUnitOfWork unitOfWork) where TRepository : class;
    }
}