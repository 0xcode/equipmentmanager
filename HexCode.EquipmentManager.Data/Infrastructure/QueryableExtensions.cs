﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace HexCode.EquipmentManager.Data.Infrastructure
{
    internal static class QueryableExtensions
    {
        public static IQueryable<TModel> Filter<TModel>(this IQueryable<TModel> query, string searchString, params Expression<Func<TModel, string>>[] propertiesToSearch)
        {
            if (string.IsNullOrEmpty(searchString) || propertiesToSearch.Length == 0)
                return query;

            Type type = typeof(TModel);
            ParameterExpression parameterExpression = Expression.Parameter(type, "x");
            Expression searchExpression = null;

            //Build up the body of the expression that will be used in the where clause
            foreach (var propertyToSearch in propertiesToSearch)
            {
                Expression propertySearchExpression = CreatePropertySearchExpression(propertyToSearch, searchString, parameterExpression);

                //Build the overall search expression by OR-ing together each column search
                if (searchExpression == null)
                    searchExpression = propertySearchExpression;
                else
                    searchExpression = Expression.Or(searchExpression, propertySearchExpression);
            }

            if (searchExpression != null)
            {
                //Create the method call
                MethodCallExpression orderByCallExpression = Expression.Call(typeof(Queryable), WhereMethodName, new Type[] {type}, query.Expression, Expression.Lambda(searchExpression, parameterExpression));

                //Call the method on the query
                query = query.Provider.CreateQuery<TModel>(orderByCallExpression);
            }

            return query;
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> query, FilteringInfo filteringInfo)
        {
            return OrderBy(query, filteringInfo.SortedProperty, filteringInfo.SortedDirection);
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> query, string propertyName, SortDirection sortDirection)
        {
            Type type = typeof(T);
            Type propertyAccessType;

            string orderByMethodName;

            if (string.IsNullOrEmpty(propertyName) == true)
                throw new ArgumentException("Cannot sort on a column with no name!");

            if (sortDirection == SortDirection.Ascending)
                orderByMethodName = "OrderBy";
            else
                orderByMethodName = "OrderByDescending";

            //Create the lambda to use in the OrderBy method call
            ParameterExpression parameter = Expression.Parameter(type, "x");
            MemberExpression propertyAccess = CreatePropertyAccessor(type, propertyName, parameter, out propertyAccessType);
            LambdaExpression orderByLambda = Expression.Lambda(propertyAccess, parameter);

            //Create the method call
            MethodCallExpression orderByCallExpression = Expression.Call(typeof(Queryable), orderByMethodName, new Type[] {type, propertyAccessType}, query.Expression, Expression.Quote(orderByLambda));

            //Call the method on the query
            return query.Provider.CreateQuery<T>(orderByCallExpression);
        }

        public static PagedList<TData> ToPagedList<TData>(this IQueryable<TData> query, PagingInfo pagingInfo)
        {
            IEnumerable<TData> items;
            int itemCount;

            if (pagingInfo.PageSize > 0)
            {
                items = query.Skip((pagingInfo.PageNumber - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize).ToList();
                itemCount = query.Count();
            }
            else
            {
                items = query.ToList();
                itemCount = items.Count();
            }

            return new PagedList<TData>(pagingInfo, itemCount, items);
        }

        private static MemberExpression CreatePropertyAccessor(Type type, string propertyName, ParameterExpression parameter, out Type propertyAccessType)
        {
            //Creates the parameter of the expression, the 'o' in 'o => ...'
            PropertyInfo propertyInfo;
            MemberExpression propertyAccess;

            //Check if we need to handle child properties
            if (propertyName.Contains('.') == true)
            {
                string[] childProperties = propertyName.Split('.');

                //Create the initial property accessor
                propertyInfo = type.GetProperty(childProperties[0]);
                if (propertyInfo == null)
                    throw new ArgumentException(string.Format("The type {0} does not contain a member {1}", type, childProperties[0]));

                propertyAccess = Expression.MakeMemberAccess(parameter, propertyInfo);

                for (int i = 1; i < childProperties.Length; i++)
                {
                    //Get the child property of the previous property
                    propertyInfo = propertyInfo.PropertyType.GetProperty(childProperties[i]);
                    if (propertyInfo == null)
                        throw new ArgumentException(string.Format("The type {0} does not contain a member {1}", propertyInfo.PropertyType, childProperties[i]));

                    //Create the child accessor
                    propertyAccess = Expression.MakeMemberAccess(propertyAccess, propertyInfo);
                }
            }
            else
            {
                propertyInfo = type.GetProperty(propertyName);
                if (propertyInfo == null)
                    throw new ArgumentException(string.Format("The type {0} does not contain a member {1}", type, propertyName));

                propertyAccess = Expression.MakeMemberAccess(parameter, propertyInfo);
            }

            //Return the return type of the selector
            propertyAccessType = propertyInfo.PropertyType;

            return propertyAccess;
        }

        private static Expression CreatePropertySearchExpression<TSource>(Expression<Func<TSource, string>> propertyToSearch, string searchString, ParameterExpression parameterExpression)
        {
            MemberExpression member = propertyToSearch.Body as MemberExpression;
            if (member == null)
                throw new ArgumentException("Expression refers to a method, not a property");

            PropertyInfo propertyInformation = member.Member as PropertyInfo;
            if (propertyInformation == null)
                throw new ArgumentException("Expression refers to a field, not a property");

            //Create the initial part of the RHS of the output expression
            //Eg. => value.
            Expression toReturn = parameterExpression;

            //Build up the RHS of the output expression based on the property passed in
            //Eg. x => x.y.z becomes => value.y.z
            foreach (string part in propertyToSearch.ToString().Split('.').Skip(1))
            {
                toReturn = Expression.PropertyOrField(toReturn, part);
            }

            toReturn = Expression.Call(toReturn, ToLowerMethodInfo);

            //Add the call to the string method for the LHS of the output expression 
            //Eg. => value.y.z.Contains("...")
            toReturn = Expression.Call(toReturn, ContainsMethodInfo, Expression.Constant(searchString));

            return toReturn;
        }

        private static readonly MethodInfo ToLowerMethodInfo = typeof(string).GetMethod("ToLower", Type.EmptyTypes);
        private static readonly MethodInfo ContainsMethodInfo = typeof(string).GetMethod("Contains", new Type[] {typeof(string)});
        private static readonly string WhereMethodName = "where";
    }
}