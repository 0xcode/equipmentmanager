﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HexCode.EquipmentManager.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;

namespace HexCode.EquipmentManager.Hub.Infrastructure
{
    public enum AjaxResultType
    {
        Success = 0,
        ValidationError = 1,
        GeneralError = 2,
        UnknownError = 3
    }

    public enum AjaxResultDataType
    {
        Json,
        Html,
        ErrorMessage
    }

    public static class AjaxResultExtensions
    {
        public static AjaxResult Ajax(this Controller controller, Result result)
        {
            return new AjaxResult(result);
        }
    }

    public class AjaxResult : ActionResult
    {
        public AjaxResult(Result result)
        {
            _resultType = (result.Value == true ? AjaxResultType.Success : AjaxResultType.GeneralError);

            if (result.Value == false)
            {
                _dataType = AjaxResultDataType.ErrorMessage;
                _data = result.ErrorMessage;
            }
        }

        public AjaxResult(ModelStateDictionary modelState)
        {
            _resultType = (modelState.IsValid == true ? AjaxResultType.Success : AjaxResultType.ValidationError);

            //Add the errors
            if (modelState.IsValid == false)
            {
                _dataType = AjaxResultDataType.Json;

                _data = modelState.ToDictionary(pair => pair.Key, pair => pair.Value.Errors.Select(e => e.ErrorMessage));
            }
        }

        public AjaxResult(AjaxResultType resultType, AjaxResultDataType dataType, object data)
        {
            _resultType = resultType;
            _dataType = dataType;
            _data = data;
        }

        private AjaxResultType _resultType;
        private AjaxResultDataType _dataType;
        private object _data;

        public override async Task ExecuteResultAsync(ActionContext context)
        {
            HttpResponse response = context.HttpContext.Response;
            response.ContentType = "application/json";

            await response.WriteAsync(JsonConvert.SerializeObject(new { result = (int)_resultType, dataType = _dataType, data = _data }));
        }

        public override void ExecuteResult(ActionContext context)
        {
            HttpResponse response = context.HttpContext.Response;
            response.ContentType = "application/json";

            response.WriteAsync(JsonConvert.SerializeObject(new { result = (int)_resultType, dataType = _dataType, data = _data })).Wait();
        }
    }
}
