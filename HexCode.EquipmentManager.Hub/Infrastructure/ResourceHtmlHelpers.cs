﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;

/*
 * Notes
 * The use of a different store for layout resources is to fix an issue with the fact that the layout code gets called after other views but they should be rendered first
 * So to fix this, if we're rendering a layout we put the resource in a different set that will be rendered before other scripts
 * Some tests were done, partial scripts render in the order they appear, it's only layouts that get rendered "out of order"
 */

namespace HexCode.EquipmentManager.Hub
{
    public static class ResourceHtmlHelpers
    {
        static ResourceHtmlHelpers()
        {
#if DEBUG
            //By default in debug we'll disable serving the minified files and enable versioned files
            ResourceHtmlHelpers.UseMinifiedScripts = false;
            ResourceHtmlHelpers.UseVersionedScripts = false;
            ResourceHtmlHelpers.UseMinifiedStyles = false;
            ResourceHtmlHelpers.UseVersionedStyles = true;
#else
            //By default outside debug we'll disable serving the minified files and enable versioned files
            ScriptHtmlHelper.UseMinifiedScripts = true;
            ScriptHtmlHelper.UseVersionedScripts = true;
            ResourceHtmlHelpers.UseMinifiedStyles = true;
            ResourceHtmlHelpers.UseVersionedStyles = true;
#endif
        }

        public static void AddDebugScript(this IHtmlHelper helper, string path)
        {
#if DEBUG
            AddScript(helper, path);
#endif
        }

        public static void AddScript(this IHtmlHelper helper, string path)
        {
            bool isLayoutResource = IsLayoutResource(helper);
            string resourceKey = (isLayoutResource == true ? LAYOUT_SCRIPT_KEY : SCRIPT_KEY);

            AddResource(helper, path, resourceKey, UseMinifiedStyles);
        }

        public static void AddDebugStyle(this IHtmlHelper helper, string path)
        {
#if DEBUG
            AddStyle(helper, path);
#endif
        }

        public static void AddStyle(this IHtmlHelper helper, string path)
        {
            bool isLayoutResource = IsLayoutResource(helper);
            string resourceKey = (isLayoutResource == true ? LAYOUT_STYLE_KEY : STYLE_KEY);

            AddResource(helper, path, resourceKey, UseMinifiedStyles);
        }

        private static void AddResource(IHtmlHelper helper, string path, string resourceKey, bool useMinifiedPath)
        {
            HashSet<string> resources;

            if (helper.ViewContext.HttpContext.Items.ContainsKey(resourceKey) == false)
            {
                resources = new HashSet<string>();
                helper.ViewContext.HttpContext.Items[resourceKey] = resources;
            }
            else
            {
                resources = helper.ViewContext.HttpContext.Items[resourceKey] as HashSet<string>;
            }

            if (useMinifiedPath == true)
            {
                //Build the minified path
                string extension = Path.GetExtension(path).Substring(1);
                string minifiedPath = Path.ChangeExtension(path, string.Format("min.{0}", extension));

                //If the minified version exists
                if (File.Exists(MapPath(helper, minifiedPath)) == true)
                    path = minifiedPath;
            }

            resources.Add(path);
        }

        private static bool IsLayoutResource(IHtmlHelper helper)
        {
            bool toReturn = false;

            if (helper.ViewContext.View is RazorView)
            {
                toReturn = ((RazorView)helper.ViewContext.View).RazorPage.IsLayoutBeingRendered;
            }

            return toReturn;
        }

        public static HtmlString RenderScripts(this IHtmlHelper helper)
        {
            StringBuilder builder = new StringBuilder();

            string tagFormat = (UseVersionedScripts == true ? VERSIONED_SCRIPT_TAG_FORMAT : UNVERSIONED_SCRIPT_TAG_FORMAT);
            string suffix = DateTime.Now.Ticks.ToString();

            //Render any layout scripts first, these will be in order
            if (helper.ViewContext.HttpContext.Items.ContainsKey(LAYOUT_SCRIPT_KEY) == true)
            {
                IEnumerable<string> layoutScripts = helper.ViewContext.HttpContext.Items[LAYOUT_SCRIPT_KEY] as HashSet<string>;
                foreach (string layoutScript in layoutScripts)
                {
                    builder.AppendFormat(tagFormat, layoutScript, suffix);
                }
            }

            //Render other scripts after
            if (helper.ViewContext.HttpContext.Items.ContainsKey(SCRIPT_KEY) == true)
            {
                IEnumerable<string> scripts = helper.ViewContext.HttpContext.Items[SCRIPT_KEY] as HashSet<string>;
                foreach (string script in scripts)
                {
                    builder.AppendFormat(tagFormat, script, suffix);
                }
            }

            return new HtmlString(builder.ToString());
        }

        public static HtmlString RenderStyles(this IHtmlHelper helper)
        {
            StringBuilder builder = new StringBuilder();

            string tagFormat = (UseVersionedStyles == true ? VERSIONED_STYLE_TAG_FORMAT : UNVERSIONED_STYLE_TAG_FORMAT);
            string suffix = DateTime.Now.Ticks.ToString();

            //Render any layout scripts first, these will be in order
            if (helper.ViewContext.HttpContext.Items.ContainsKey(LAYOUT_STYLE_KEY) == true)
            {
                IEnumerable<string> styles = helper.ViewContext.HttpContext.Items[LAYOUT_STYLE_KEY] as HashSet<string>;
                foreach (string style in styles)
                {
                    string relAttribute = Path.GetExtension(style).TrimStart('.') == "less" ? LESS_REL_ATTRIBUTE : CSS_REL_ATTRIBUTE;
                    builder.AppendFormat(tagFormat, relAttribute, style, suffix);
                }
            }

            //Render other scripts after
            if (helper.ViewContext.HttpContext.Items.ContainsKey(STYLE_KEY) == true)
            {
                IEnumerable<string> styles = helper.ViewContext.HttpContext.Items[STYLE_KEY] as HashSet<string>;
                foreach (string style in styles)
                {
                    builder.AppendFormat(tagFormat, style, suffix);
                }
            }

            return new HtmlString(builder.ToString());
        }

        private static string MapPath(IHtmlHelper helper, string path)
        {
            IHostingEnvironment environment = ((IHostingEnvironment)helper.ViewContext.HttpContext.RequestServices.GetService(typeof(IHostingEnvironment)));

            if (path.StartsWith("~") == true)
                path = path.Substring(1);

            if (path.StartsWith("/") == true)
                path = path.Substring(1);

            return Path.Combine(environment.WebRootPath, path.Replace('/', Path.DirectorySeparatorChar));
        }

        private const string SCRIPT_KEY = "__SCRIPTS__";
        private const string LAYOUT_SCRIPT_KEY = "__LAYOUT_SCRIPTS__";
        private const string VERSIONED_SCRIPT_TAG_FORMAT = "<script type=\"text/javascript\" src=\"{0}?v={1}\"></script>\r\n";
        private const string UNVERSIONED_SCRIPT_TAG_FORMAT = "<script type=\"text/javascript\" src=\"{0}?v={1}\"></script>\r\n";

        private const string STYLE_KEY = "__STYLES__";
        private const string LAYOUT_STYLE_KEY = "__LAYOUT_STYLES__";
        private const string VERSIONED_STYLE_TAG_FORMAT = "<link rel=\"{0}\" href=\"{1}?v={2}\" />\r\n";
        private const string UNVERSIONED_STYLE_TAG_FORMAT = "<link rel=\"{0}\" href=\"{1}\" />\r\n";
        private const string CSS_REL_ATTRIBUTE = "stylesheet";
        private const string LESS_REL_ATTRIBUTE = "stylesheet/less";

        private static bool UseMinifiedScripts { get; set; }
        private static bool UseVersionedScripts { get; set; }
        private static bool UseMinifiedStyles { get; set; }
        private static bool UseVersionedStyles { get; set; }
    }
}
