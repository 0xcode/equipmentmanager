﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HexCode.EquipmentManager.Hub.Infrastructure
{
    public class ModelBuilder
    {
        public static string BuildModel<TModel>(TModel source)
        {
            CodeBuilder builder = new CodeBuilder();

            Type sourceType = typeof(TModel);

            builder.WriteFormattedLine("var {0} = function() {{", sourceType.Name);
            builder.IncreaseIndent();
            builder.WriteLine("var self = this;");

            foreach (PropertyInfo property in sourceType.GetProperties())
            {
                if (property.PropertyType == typeof(string) || property.PropertyType == typeof(char))
                {
                    builder.WriteFormattedLine("self.{0} = ko.observable();", property.Name);
                }
                else if (property.PropertyType == typeof(int) || property.PropertyType == typeof(double) || property.PropertyType == typeof(float))
                {
                    builder.WriteFormattedLine("self.{0} = ko.observable(0);", property.Name);
                }
            }

            builder.DecreaseIndent();
            builder.WriteLine("}};");

            return builder.ToString();
        }
    }

    public class CodeBuilder
    {
        public CodeBuilder()
        {
            _builder = new StringBuilder();
            _indentLevel = 0;
        }

        public CodeBuilder IncreaseIndent()
        {
            _indentLevel++;

            return this;
        }

        public CodeBuilder DecreaseIndent()
        {
            _indentLevel = Math.Max(_indentLevel - 1, 0);

            return this;
        }

        public CodeBuilder WriteLine(string value)
        {
            AppendIndent();

            _builder.AppendLine(value);

            return this;
        }

        public CodeBuilder WriteFormattedLine(string format, params object[] parameters)
        {
            AppendIndent();

            _builder.AppendFormat(format, parameters).AppendLine();

            return this;
        }

        private void AppendIndent()
        {
            _builder.Append('\t', _indentLevel);
        }

        public override string ToString()
        {
            return _builder.ToString();
        }

        private int _indentLevel;
        private readonly StringBuilder _builder;
    }
}
