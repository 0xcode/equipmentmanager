﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HexCode.EquipmentManager.Hub.Infrastructure
{
    public interface IViewToDomainMapper<TViewModel, TDomainModel>
    {
        TDomainModel MapToDomainModel(TViewModel viewModel);
        IEnumerable<TDomainModel> MapToDomainModel(IEnumerable<TViewModel> viewModelList);
        void MapToDomainModel(TViewModel viewModel, TDomainModel domainModel);
    }

    public interface IDomainToViewMapper<TViewModel, TDomainModel>
    {
        TViewModel MapToViewModel(TDomainModel domainModel);
        IEnumerable<TViewModel> MapToViewModel(IEnumerable<TDomainModel> domainModelList);
        void MapToViewModel(TDomainModel domainModel, TViewModel viewModel);
    }

    public abstract class BaseViewToDomainMapper<TViewModel, TDomainModel> : IViewToDomainMapper<TViewModel, TDomainModel>
    {
        public TDomainModel MapToDomainModel(TViewModel viewModel)
        {
            TDomainModel toReturn = CreateDomainModel();

            MapToDomainModel(viewModel, toReturn);

            return toReturn;
        }

        public IEnumerable<TDomainModel> MapToDomainModel(IEnumerable<TViewModel> viewModelList)
        {
            return viewModelList.Select(m => MapToDomainModel(m));
        }

        public abstract void MapToDomainModel(TViewModel viewModel, TDomainModel domainModel);
        protected abstract TDomainModel CreateDomainModel();
    }

    public abstract class BaseBidirectionalMapper<TViewModel, TDomainModel> : IViewToDomainMapper<TViewModel, TDomainModel>, IDomainToViewMapper<TViewModel, TDomainModel>
    {
        public TDomainModel MapToDomainModel(TViewModel viewModel)
        {
            TDomainModel toReturn = CreateDomainModel();

            MapToDomainModel(viewModel, toReturn);

            return toReturn;
        }

        public IEnumerable<TDomainModel> MapToDomainModel(IEnumerable<TViewModel> viewModelList)
        {
            return viewModelList.Select(m => MapToDomainModel(m));
        }

        public TViewModel MapToViewModel(TDomainModel domainModel)
        {
            TViewModel toReturn = CreateViewModel();

            MapToViewModel(domainModel, toReturn);

            return toReturn;
        }

        public IEnumerable<TViewModel> MapToViewModel(IEnumerable<TDomainModel> domainModelList)
        {
            return domainModelList.Select(m => MapToViewModel(m));
        }

        public abstract void MapToDomainModel(TViewModel viewModel, TDomainModel domainModel);
        public abstract void MapToViewModel(TDomainModel domainModel, TViewModel viewModel);
        protected abstract TDomainModel CreateDomainModel();
        protected abstract TViewModel CreateViewModel();
    }
}