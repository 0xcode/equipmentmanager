﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HexCode.EquipmentManager.Data.SearchCriteria;
using HexCode.EquipmentManager.Hub.Models.Shared;

namespace HexCode.EquipmentManager.Hub.Models.UserManagement
{
    public class UserTableStateViewModel : TableStateViewModel
    {
        public UserFilteringInfo.UserStateFilter StateFilter { get; set; }

        public UserFilteringInfo FilteringInfo
        {
            get
            {
                UserFilteringInfo filteringInfo = new UserFilteringInfo()
                {
                    UserState = this.StateFilter
                };

                base.PopulateFilteringInfo(filteringInfo);

                return filteringInfo;
            }
        }
    }
}
