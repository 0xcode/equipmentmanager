﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using LocalResources = HexCode.EquipmentManager.Hub.Resources.UserManagement.Create;

namespace HexCode.EquipmentManager.Hub.Models.UserManagement
{
    public class CreateViewModel
    {
        [Display(ResourceType = typeof(LocalResources), Name = "FirstNameLabel")]
        [Required(ErrorMessageResourceType = typeof(LocalResources), ErrorMessageResourceName = "FirstNameEmptyError")]
        public string FirstName { get; set; }

        [Display(ResourceType = typeof(LocalResources), Name = "LastNameLabel")]
        [Required(ErrorMessageResourceType = typeof(LocalResources), ErrorMessageResourceName = "LastNameEmptyError")]
        public string LastName { get; set; }

        [Display(ResourceType = typeof(LocalResources), Name = "EmailLabel")]
        [Required(ErrorMessageResourceType = typeof(LocalResources), ErrorMessageResourceName = "EmailEmptyError")]
        [EmailAddress(ErrorMessageResourceType = typeof(LocalResources), ErrorMessageResourceName = "EmailFormatError")]
        public string Email { get; set; }
    }
}
