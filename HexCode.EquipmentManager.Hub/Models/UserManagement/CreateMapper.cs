﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HexCode.EquipmentManager.Core.Models;
using HexCode.EquipmentManager.Hub.Infrastructure;

namespace HexCode.EquipmentManager.Hub.Models.UserManagement
{
    public class CreateMapper : BaseViewToDomainMapper<CreateViewModel, User>
    {
        public override void MapToDomainModel(CreateViewModel viewModel, User domainModel)
        {
            domainModel.FirstName = viewModel.FirstName;
            domainModel.LastName = viewModel.LastName;
        }

        protected override User CreateDomainModel()
        {
            return new User();
        }
    }
}
