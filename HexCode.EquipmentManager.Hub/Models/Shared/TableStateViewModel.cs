﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HexCode.EquipmentManager.Data;
using HexCode.EquipmentManager.Data.Infrastructure;

namespace HexCode.EquipmentManager.Hub.Models.Shared
{
    public class TableStateViewModel
    {
        public string SortedColumn { get; set; }
        public SortDirection SortedDirection { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string FilterText { get; set; }

        public PagingInfo PagingInfo
        {
            get
            {
                return new PagingInfo(this.PageSize, this.PageNumber);
            }
        }

        protected void PopulateFilteringInfo(FilteringInfo filteringInfo)
        {
            filteringInfo.FilterText = this.FilterText;
            filteringInfo.SortedProperty = this.SortedColumn;
            filteringInfo.SortedDirection = this.SortedDirection;
        }
    }
}
