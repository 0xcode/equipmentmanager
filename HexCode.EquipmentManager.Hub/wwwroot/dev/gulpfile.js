﻿var gulp = require("gulp");
var runSequence = require("run-sequence");
var requireDir = require("require-dir");
var semanticBuildTasks = requireDir("./semantic-ui/tasks/build");
var semanticConfig = require("./semantic-ui/tasks/config/user");
var uglifyJS = require("gulp-uglify");
var rename = require("gulp-rename");
var changed = require("gulp-changed");
var less = require("gulp-less");
var uglifyCSS = require("gulp-uglifycss");
var fs = require('fs');
var path = require('path');

function isSourceNewer(source, destination) {
    var toReturn = true;

    //If the destination exists we need to compare the files times
    if (fs.existsSync(destination) === true) {
        var sourceStat = fs.statSync(source);
        var destinationStat = fs.statSync(destination);

        //If the source is actually a directory we need to recursively check the file times
        if (sourceStat.isDirectory() === true) {
            toReturn = isDirectoryNewer(source, destinationStat.mtime);
        } else {
            toReturn = sourceStat.mtime > destinationStat.mtime;
        }
    }

    return toReturn;
}

/**
 * Recursively goes through the given source directory and returns if any files are never than the given date
 * @param {any} directory The directory to check
 * @param {Date} timestamp The timestamp to compare against
 * @returns {boolean} If the directory is newer than the comparison date
 */
function isDirectoryNewer(directory, timestamp) {
    var toReturn = false;
    var files = fs.readdirSync(directory);

    for (var i = 0; i < files.length; i++) {
        var file = path.join(directory, files[i]);
        var fileStat = fs.statSync(file);

        if (fileStat.isDirectory() === true) {
            toReturn = isDirectoryNewer(file, timestamp);
        } else {
            toReturn = fileStat.mtime > timestamp;
        }

        //We could put this as part of the for definition but this is more readable
        if (toReturn === true)
            break;
    }

    return toReturn;
}

//Semantic UI tasks pulled from the semantic build files
gulp.task("prepare-semantic-css", semanticBuildTasks.css);
gulp.task("prepare-semantic-js", semanticBuildTasks.javascript);

gulp.task("move-semantic-js", function () {
    var sources = semanticConfig.paths.output.packaged + "/*.js";

    return gulp.src(sources).pipe(gulp.dest("../js/Shared"));
});

gulp.task("move-semantic-css", function () {
    var sources = semanticConfig.paths.output.packaged + "/*.css";

    return gulp.src(sources).pipe(gulp.dest("../css"));
});

//Minify js files
gulp.task("minify-js", function () {
    var destination = "../js";

    //Minify only files that have changed
    return gulp.src(["../js/**/*.js", "!../js/**/*.min.js"])
        .pipe(changed(destination, { extension: ".min.js" })) //Checks if the min file is out of date (older than the source)
        .pipe(uglifyJS())
        .pipe(rename({ suffix: ".min" }))
        .pipe(gulp.dest(destination));
});

//Compile and minify less files
gulp.task("compile-less", function () {
    var destination = "../css";

    //Minify only files that have changed
    return gulp.src("../css/**/*.less")
        .pipe(changed(destination, { extension: ".min.css" })) //Checks if the min file is out of date (older than the source)
        .pipe(less())
        .pipe(uglifyCSS())
        .pipe(rename({ suffix: ".min" }))
        .pipe(gulp.dest(destination));
});

gulp.task("minify-css", function () {
    var destination = "../css";

    //Minify only files that have changed
    return gulp.src(["../css/**/*.css", "!../css/**/*.min.css"])
        .pipe(changed(destination, { extension: ".min.css" })) //Checks if the min file is out of date (older than the source)
        .pipe(uglifyCSS())
        .pipe(rename({ suffix: ".min" }))
        .pipe(gulp.dest(destination));
});

gulp.task("compile-semantic-js", function (callback) {
    if (isSourceNewer("./semantic-ui/src", "../js/Shared/semantic.js") === true) {
        runSequence("prepare-semantic-js", "move-semantic-js", callback);
    } else {
        callback();
    }
});

gulp.task("compile-semantic-css", function (callback) {
    if (isSourceNewer("./semantic-ui/src", "../css/semantic.css") === true) {
        runSequence("prepare-semantic-css", "move-semantic-css", callback);
    } else {
        callback();
    }
});

gulp.task("build-js", function (callback) {
    runSequence("compile-semantic-js", "minify-js", callback);
});

gulp.task("build-css", function (callback) {
    runSequence("compile-semantic-css", "compile-less", "minify-css", callback);
});
