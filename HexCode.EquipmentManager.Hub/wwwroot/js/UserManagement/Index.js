﻿
function TableStateViewModel() {
    var pagerSize = 5;
    var self = this;
    var updateEnabled = true;

    self.pageCount = ko.observable(1);
    self.pageNumber = ko.observable(1);
    self.pageSize = ko.observable(5);
    self.sortedColumn = ko.observable("");
    self.sortedDirection = ko.observable(0);

    self.pages = ko.computed(function () {
        var currentPage = self.pageNumber();
        var totalPages = self.pageCount();

        var offset = Math.floor((pagerSize - 1) / 2);
        var rangeStart;
        var rangeEnd;

        if ((currentPage - offset) <= 0) {
            //We're close to the start, we want to show more buttons on right
            rangeStart = 1;
            rangeEnd = Math.min(currentPage + ((pagerSize - 1) - (currentPage - rangeStart)), totalPages);
        } else if (currentPage + offset > totalPages) {
            //We're close to the end so we want to show more of the left
            rangeEnd = totalPages;
            rangeStart = Math.max(currentPage - ((pagerSize - 1) - (rangeEnd - currentPage)), 1);
        } else {
            rangeStart = currentPage - offset;
            rangeEnd = currentPage + offset;
        }

        return ko.utils.range(rangeStart, rangeEnd);
    }, self);

    self.isFirstPage = ko.computed(function() {
        return self.pageNumber() === 1;
    }, self);

    self.isLastPage = ko.computed(function () {
        return self.pageNumber() === self.pageCount();
    }, self);

    self.goToFirst = function() {
        self.pageNumber(1);
        onStateChanged();
    };

    self.goToNext = function() {
        self.pageNumber(Math.min(self.pageNumber() + 1, self.pageCount()));
        onStateChanged();
    };

    self.goToPage = function(page) {
        self.pageNumber(page);
        onStateChanged();
    };

    self.goToPrevious = function () {
        self.pageNumber(Math.max(self.pageNumber() - 1, 1));
        onStateChanged();
    };

    self.goToLast = function() {
        self.pageNumber(self.pageCount());
        onStateChanged();
    };

    self.suspendUpdates = function() {
        updateEnabled = false;
    };

    self.resumeUpdates = function() {
        updateEnabled = true;
    };

    self.getStateData = function() {
        return {
            pageNumber: self.pageNumber(),
            pageSize: self.pageSize(),
            sortedColumn: self.sortedColumn(),
            sortedDirection: self.sortedDirection()
        };
    };

    self.stateChanged = function() {
    };

    function onStateChanged() {
        if (updateEnabled === true) {
            self.stateChanged();
        }
    }
}

function UserListItemViewModel(data) {
    this.id = data.id;
    this.firstName = data.firstName;
    this.lastName = data.lastName;
    this.isActive = data.isActive;
}

function IndexViewModel(listUrl, createUrl) {
    var self = this;
    TableStateViewModel.call(self); //Extend the table model

    self.listUrl = listUrl;
    self.isLoading = ko.observable(false);
    self.users = ko.observableArray();
    
    self.createUser = function() {
        showRemoteDialog(createUrl);
    };

    loadData();

    function loadData() {
        self.isLoading(true);

        $.post(listUrl, self.getStateData(), function (data) {
            self.users(data.items);

            self.suspendUpdates();
            self.pageNumber(data.pageNumber);
            self.pageCount(data.pageCount);
            self.resumeUpdates();

            self.isLoading(false);
        });
    }

    self.stateChanged = loadData;
}