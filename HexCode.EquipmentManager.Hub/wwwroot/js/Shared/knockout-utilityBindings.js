﻿closure([jQuery, ko, window], function ($, ko, window) {
    ko.bindingHandlers.loading = {
        init: function (element, valueAccessor) {
            var value = ko.unwrap(valueAccessor());
            element = $(element);

            //If there isn't a custom dimmer add one
            if ($(".ui.dimmer", element).length === 0) {
                $('<div class="ui inverted dimmer"><div class="ui text loader"></div></div>').appendTo(element);
            }

            //Setup the dimmer so it doesn't close when clicked
            element.dimmer({ closable: false });

            //If the initial value is true show the dimmer
            if (value === true) {
                element.dimmer("show");
            }
        },
        update: function (element, valueAccessor) {
            var value = ko.unwrap(valueAccessor());
            element = $(element);

            if (value === true) {
                element.dimmer("show");
            } else {
                element.dimmer("hide");
            }
        }
    };

    ko.bindingHandlers.openDialog = {
        init: function(element, valueAccessor) {
            element = $(element);

            element.on("click", function() {
                var parameters = ko.unwrap(valueAccessor());
                var url = ko.unwrap(parameters.url);
                var data = ko.unwrap(parameters.data);

                showModalDialog(url, data);
            });
        } 
    };

    ko.bindingHandlers.submitDialog = {
        init: function (element, valueAccessor, allBindings, viewModel) {
            element = $(element);

            element.on("click", function () {
                var parameters = ko.unwrap(valueAccessor());
                var url = ko.unwrap(parameters.url);
                var method = ko.unwrap(parameters.method) || "POST";
                var data = ko.unwrap(parameters.data) || ko.toJS(viewModel);
                var successCallback = parameters.success || $.noop;
                var errorCallback = parameters.error || $.noop;
                var messages = parameters.messages;

                $.ajax({
                    url: url,
                    method: method,
                    data: data,
                    success: function (response) {
                        if (response.result === 0) { //Success
                            //Call the callback, if it doesn't return false close the dialog
                            if (successCallback(response.data) !== false) {
                                element.closest(".ui.modal").modal("hide");
                            }
                        } else if (response.result === 1 && messages !== undefined) {
                            var errors = [];

                            for (var key in response.data) {
                                errors = errors.concat(response.data[key]);
                            }

                            messages(errors);
                        } else if (response.result === 2 && messages !== undefined) {
                            messages(response.data);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        errorCallback(errorThrown || textStatus);
                    }

                });
            });
        }
    }

    ko.bindingHandlers.enabled = {
        update: function (element, valueAccessor) {
            var value = ko.unwrap(valueAccessor());
            element = $(element);

            if (value === true) {
                element.removeClass("disabled");
            } else {
                element.addClass("disabled");
            }
        }
    }

    //Custom events so we can tie KO into Semantic's form validation
    $.fn.form.settings.onValid = function () {
        var input = this;
        input.trigger("valid");
    }

    $.fn.form.settings.onInvalid = function () {
        var input = this;
        input.trigger("invalid");
    }

    ko.bindingHandlers.formValidation = {
        init: function(element, valueAccessor, allBindings, viewModel) {
            element = $(element);
            var value = valueAccessor();

            //We need to track which fields are invalid so we add that to the view model
            if (viewModel._invalidFields_ === undefined) {
                viewModel._invalidFields_ = new Set();
            }

            element.on("valid", function(e) {
                viewModel._invalidFields_.delete($(e.target).attr("name"));
                value(viewModel._invalidFields_.size === 0);
            });

            element.on("invalid", function (e) {
                viewModel._invalidFields_.add($(e.target).attr("name"));
                value(viewModel._invalidFields_.size === 0);
            });
        }
    }
});
