﻿(function (window, undefined) {
    /**
     * Creates a function closure using the given function and any optional parameters
     * @param {(function|any)} p1 The closure function or arguments to be passed into the closure function
     * @param {function} p2 The closure function
     * @example
     * closure([jQuery, window], function ($, window, undefined) {
     *      this.func1 = function() {...}; //Will be in window, can be called func1();
     *
     *      function func2() {...} //Will not be accessible outside this function
     * });
     */
    window.closure = function(p1, p2) {
        var closureFunction;
        var closureArguments;

        if (p2 !== undefined && typeof p2 === "function") {
            closureFunction = p2;
            closureArguments = p1;
        } else if (p1 !== undefined && typeof p1 === "function") {
            closureArguments = [];
            closureFunction = p1;
        } else {
            throw "Invalid arguments to closure";
        }

        closureFunction.apply(window, closureArguments);
    };

    /**
     * Creates a closure function within the given namespace. Variables assigned to the this pointer will be public in the namespace
     * @param {string} name The name of the namespace
     * @param {(function|any)} p1 The closure function or arguments to be passed into the closure function
     * @param {function} p2 The closure function
     * @example
     * namespace("test.name.space", [jQuery, window], function ($, window, undefined) {
     *      this.func1 = function() {...}; //Will be in window.test.name.space, can be called test.name.space.func1();
     *
     *      function func2() {...} //Will not be accessible outside this function
     * });
     *
     */
    window.namespace = function(name, p1, p2) {
        var closureFunction;
        var closureArguments;

        if (p2 !== undefined && typeof p2 === "function") {
            closureFunction = p2;
            closureArguments = p1;
        } else if (p1 !== undefined && typeof p1 === "function") {
            closureArguments = [];
            closureFunction = p1;
        } else {
            throw "Invalid arguments to namespace";
        }

        var namespaceContainer = createNamespaceContainer(name, window);

        closureFunction.apply(namespaceContainer, closureArguments);
    };

    function createNamespaceContainer(namespace, root) {
        var parts = namespace.split(".");
        var currentObject = root;

        for (var i = 0; i < parts.length; i++) {
            var part = parts[i];

            if (currentObject[part] === undefined) {
                currentObject[part] = {};
            }

            currentObject = currentObject[part];
        }

        return currentObject;
    }
})(window);