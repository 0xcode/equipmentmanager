﻿closure([jQuery], function ($, undefined) {
    //list of dialog events
    var dialogEvents = {
        loaded: "loaded.dialog"
    };

    //Create a list of events
    this.events = $.extend({}, this.events, Object.freeze({ dialog: dialogEvents }));

    this.showRemoteDialog = function (url, data) {
        $.ajax({
            url: url,
            data: data,
            success: function (response) {
                var container = getRemoteDialogContainer();

                //Set the content and trigger the loaded event
                container.html(response);
                container.trigger(dialogEvents.loaded);

                //Setup the modal
                $(".modal", container).modal({ closable: false, detachable: false, selector: { approve: ".ok", deny: ".cancel" } }).modal("show");
            }
        });
    };

    this.showErrorDialog = function (message) {

    }

    function getRemoteDialogContainer() {
        var remoteDialogContainer = $("#__REMOTE_DIALOG__", "body");

        if (remoteDialogContainer.length === 0) {
            remoteDialogContainer = $("<div id='__REMOTE_DIALOG__'></div>").appendTo("body");
        }

        return remoteDialogContainer;
    }

    function getErrorDialog() {

    }
})