﻿(function ($, ko, undefined) {
    ko.addAutomaticBindingEvent = function (events) {
        //Adds a listener that will perform automatic binding when the event triggers
        $(document).on(events, function (e) {
            $("[data-model]", e.target).each(function () {
                ko.applyAutomaticBinding($(this));
            });
        });
    };

    ko.applyAutomaticBinding = function (element) {
        var modelName = element.data("model");

        if (window[modelName] !== undefined) {
            //Get the model parameters based on their names in the constructor, the first value will be null, see the SO article for why
            var modelParameters = buildParameterArray(element.data(), window[modelName]);

            //See https://stackoverflow.com/a/8843181
            var model = new (window[modelName].bind.apply(window[modelName], modelParameters))();

            if (element.data("bindInputs") === true) {
                bindInputs(element, model);
            }

            ko.applyBindings(model, element[0]);
        } else {
            throw "Unknown model: " + modelName;
        }
    };

    $(function () {
        //Find any element with a model attribute and use it to create new models
        $("[data-model]").each(function() {
            ko.applyAutomaticBinding($(this));
        });
    });

    function buildParameterArray(data, constructor) {
        var parameterNames = getParameterNames(constructor);

        //When we make the call to create out model we need to the first parameter to be null, instead of doing a slice it's more efficient to do it here
        var parameterArray = new Array(parameterNames.length + 1);
        parameterArray[0] = null;

        for (var i = 0; i < parameterNames.length; i++) {
            //Get the parameter name and convert it to match what it would look like as a data attribute (eg. data-arg-url => argUrl)
            var parameterName = parameterNames[i];
            parameterName = "arg" + parameterName.charAt(0).toUpperCase() + parameterName.slice(1);

            parameterArray[i + 1] = data[parameterName];
        }

        return parameterArray;
    }

    function getParameterNames(functionDefinition) {
        var functionString = functionDefinition.toString().replace(COMMENTS_REGEX, "");
        var parametersString = functionString.substring(functionString.indexOf("(") + 1, functionString.indexOf(")"));

        var matches = parametersString.match(ARGUMENT_REGEX);

        if (matches === null)
            matches = [];

        return matches;
    }

    function bindInputs(container, model) {
        //Get any inputs and automagically create matching observables and apply bindings
        $(":input", container).each(function () {
            var input = $(this);
            var name = input.attr("name");
            var value = input.val();

            //Convert the name to lower camelcase
            name = name.charAt(0).toLowerCase() + name.slice(1);

            model[name] = ko.observable(value);

            if (input.data("bind") === undefined && input.data("ignore") !== true) {
                ko.applyBindingsToNode(input[0], { value: model[name] }, model);
            }
        });
    }

    var COMMENTS_REGEX = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
    var ARGUMENT_REGEX = /([^\s,]+)/g;
})(jQuery, ko);