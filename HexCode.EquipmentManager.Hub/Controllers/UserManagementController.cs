﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HexCode.EquipmentManager.Core.Models;
using HexCode.EquipmentManager.Data;
using HexCode.EquipmentManager.Data.Infrastructure;
using HexCode.EquipmentManager.Data.Services;
using HexCode.EquipmentManager.Hub.Infrastructure;
using HexCode.EquipmentManager.Hub.Models.UserManagement;
using HexCode.EquipmentManager.Hub.Models.Shared;
using Microsoft.AspNetCore.Mvc;

namespace HexCode.EquipmentManager.Hub.Controllers
{
    public class UserManagementController : Controller
    {
        private readonly IUserService _userService;

        public UserManagementController(IUserService userService)
        {
            _userService = userService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView("CreateDialog");
        }

        [HttpPost]
        public IActionResult Create(CreateViewModel viewModel)
        {
            IActionResult toReturn;

            if (this.ModelState.IsValid == true)
            {
                CreateMapper mapper = new CreateMapper();
                User domainModel = mapper.MapToDomainModel(viewModel);

                Result result = _userService.Create(domainModel);

                toReturn = new AjaxResult(result);
            }
            else
            {
                toReturn = new AjaxResult(this.ModelState);
            }

            return toReturn;
        }

        [HttpPost]
        public IActionResult UserList(UserTableStateViewModel tableState)
        {
            PagedList<User> result =_userService.List(tableState.PagingInfo, tableState.FilteringInfo);

            return Json(new {pageCount = result.PageCount, pageSize = tableState.PageSize, pageNumber = result.PageNumber, totalItems = result.ItemCount, items = result.Items});
        }
    }
}